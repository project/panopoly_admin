/**
 * @file
 * Javascript for the Block Library form.
 */

(function ($, Drupal) {

  Drupal.behaviors.panopolyAdminBlockLibrary = {
    attach: function (context, settings) {
      var $show_advanced = $('[data-drupal-selector="edit-show-advanced-blocks"]', context);
      if ($show_advanced.length > 0) {
        var update_advanced_blocks = function () {
          var show = $(this).is(':checked');
          $('.js-panopoly-admin-advanced-block-wrapper').toggle(show);
          $('.js-panopoly-admin-hidden-block-count').toggle(!show);
        };
        $(once('panopoly-admin-block-library', $show_advanced))
          .each(update_advanced_blocks)
          .on('change', update_advanced_blocks);
      }

      var $enable_category = $('.js-panopoly-admin-category-checkbox', context);
      if ($enable_category.length > 0) {
        var toggle_category_blocks = function () {
          var $self = $(this),
            enabled = $self.is(':checked'),
            $category = $self.closest('details').find('.js-panopoly-admin-block-category');
          $category.toggle(enabled);
        }
        $(once('panopoly-admin-block-library', $enable_category))
          .each(toggle_category_blocks)
          .on('change', toggle_category_blocks);
      }
    }
  };

})(jQuery, Drupal);
