<?php

namespace Drupal\panopoly_admin\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Alters Layout Builder's choose block controller.
 */
class AlterChooseBlockEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Priority > 0 so that it runs before the controller output
    // is rendered by \Drupal\Core\EventSubscriber\MainContentViewSubscriber.
    $events[KernelEvents::VIEW][] = ['onView', 50];
    return $events;
  }

  /**
   * Alters the result of Layout Builder's choose block controller.
   *
   * @param \Symfony\Component\HttpKernel\Event\ViewEvent $event
   *   The event.
   */
  public function onView(ViewEvent $event) {
    if (!\Drupal::currentUser()->hasPermission('use blocks hidden by panopoly_admin')) {
      return;
    }

    $request = $event->getRequest();
    $route = $request->attributes->get('_route');

    if ($route === 'layout_builder.choose_block') {
      $build = $event->getControllerResult();
      if (is_array($build)) {

        /** @var \Drupal\layout_builder\SectionStorageInterface $section_storage */
        $section_storage = $request->attributes->get('section_storage');
        $delta = $request->attributes->get('delta');
        $region = $request->attributes->get('region');
        $show_hidden_blocks = $request->query->get('panopoly_admin_show_hidden_blocks', FALSE);

        $link = [
          '#type' => 'link',
          '#url' => Url::fromRoute('layout_builder.choose_block',
            [
              'section_storage_type' => $section_storage->getStorageType(),
              'section_storage' => $section_storage->getStorageId(),
              'delta' => $delta,
              'region' => $region,
              'panopoly_admin_show_hidden_blocks' => !$show_hidden_blocks,
            ]
          ),
          '#title' => $show_hidden_blocks ? $this->t('Restore default blocks') : $this->t('Show hidden blocks'),
          '#attributes' => [
            'class' => ['use-ajax', 'panopoly-admin-show-hidden-blocks-link'],
            'data-dialog-type' => 'dialog',
            // This will be altered by layout_builder_model if present!
            'data-dialog-renderer' => 'off_canvas',
          ],
        ];

        if (isset($build['block_categories']['categories'])) {
          // This means we're using Panopoly Magic's version of the controller.
          $build['block_categories']['categories']['panopoly_admin_show_hidden_blocks'] = $link;
        }
        else {
          // This is with the default Layout Builder controller.
          $build['panopoly_admin_link'] = $link;
        }

        $build['#attached']['library'][] = 'panopoly_admin/choose_block';

        $event->setControllerResult($build);
      }
    }
  }

}
